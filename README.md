# Notes

JSX is just a syntactic improvement of React.createElement()
hardcoding values is directly setting a value to the property
dynamically setting values is setting a variable to a property exp: ```<a href={this.props.path} ...></a>```

the JSX needs to be compiled. Browsers can’t run JSX —they can run
only JavaScript, so you need to take the JSX and transpile it to normal JS
transpilation (from compilation and transformation)

Webpack is a module bundler [Source](https://www.sitepoint.com/beginners-guide-to-webpack-2-and-module-bundling/)
sass-loader transforms Sass into CSS.
css-loader parses the CSS into JavaScript and resolves any dependencies.
style-loader outputs our CSS into a ```<style>``` tag in the document.
You can think of these as function calls, the output of one loader feeds as input into the next.

### styleLoader(cssLoader(sassLoader('source')));


React and JSX accept any attribute that’s a standard HTML attribute, except class and
for . Those names are reserved words in JavaScript/ ECMAS cript, and JSX is converted
into regular JavaScript. Use className and htmlFor instead.

```false``` && ```0``` && ```""``` (empty string) && ```null``` && ```Undefined``` && ```NaN``` (not a number) == false value

JSX is just syntactic sugar for React methods like createElement .
You should use className and htmlFor instead of the standard HTML class
and for attributes.
The style attribute takes a JavaScript object, not a string like normal HTML .
Ternary operators and IIFE are the best ways to implement if / else statements.
Outputting variables, comments, and HTML entities, and compiling JSX code
into native JavaScript are easy.
There are a few choices to turn JSX into regular JavaScript; compiling with the
Babel CLI requires minimal setup compared to configuring build processing
with a tool like Gulp or Webpack or writing Node/JavaScript scripts to use the
Babel API .

As mentioned above we'll use Yarn Package Manager. It is quite similar to npm and has almost the same commands provided by npm. It also comes with a few extra features that npm does not provide. Just to catch you up, a few of the main reasons I use yarn are:

If you had already installed a package before, you can install it again without any internet connection. Apart from the fact that you can install packages offline, this also increases the speed of your installments.
The same exact dependencies are installed on any machine. This essentially means that an install on one machine will work the same exact way on any other machine.

## React Projects:

React in Action p11 

1. facebook website
2. Atlassian projects like Trello, Confluence
3. Goto conference website
4. N26 bank
5. dropbox
6. Visual Studio Team Services (VSTS)
7. Auth0 website
8. sitepoint
9. udemy website
10. coursera website
11. npmjs website
