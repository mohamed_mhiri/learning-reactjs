/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var head = document.getElementsByTagName('head')[0];
/******/ 				var script = document.createElement('script');
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = __webpack_require__.p + "" + ({}[chunkId]||chunkId) + ".bundle.js";
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				function onScriptComplete(event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							var error = new Error('Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')');
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./app.js","common"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app.js":
/*!****************!*\
  !*** ./app.js ***!
  \****************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _collection = __webpack_require__(/*! lodash/collection */ \"../node_modules/lodash/collection.js\");\n\nvar _people = __webpack_require__(/*! ./people */ \"./people.js\");\n\nvar _people2 = _interopRequireDefault(_people);\n\n__webpack_require__(/*! ./style.scss */ \"./style.scss\");\n\nvar _code = __webpack_require__(/*! ./code.png */ \"./code.png\");\n\nvar _code2 = _interopRequireDefault(_code);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// src/app.js\nvar img = document.createElement('img');\nimg.src = _code2.default;\nimg.style.backgroundColor = '#2B3A42';\nimg.style.padding = '20px';\nimg.width = 32;\ndocument.body.appendChild(img);\n\nvar managerGroups = (0, _collection.groupBy)(_people2.default, 'manager');\n\nvar content = document.querySelector('#content');\ncontent.innerHTML = '<pre>' + JSON.stringify(managerGroups, null, 2) + '</pre>';\n\nvar routes = {\n  dashboard: function dashboard() {\n    Promise.all(/*! import() */[__webpack_require__.e(\"common\"), __webpack_require__.e(0)]).then(function() { var module = __webpack_require__(/*! ./dashboard */ \"./dashboard.js\"); return typeof module === \"object\" && module && module.__esModule ? module : Object.assign({/* fake namespace object */}, typeof module === \"object\" && module, { \"default\": module }); }).then(function (dashboard) {\n      dashboard.draw();\n    }).catch(function (err) {\n      console.log('Chunk loading failed');\n    });\n  }\n\n  // demo async loading with a timeout\n};setTimeout(routes.dashboard, 1000);\n\n//# sourceURL=webpack:///./app.js?");

/***/ }),

/***/ "./code.png":
/*!******************!*\
  !*** ./code.png ***!
  \******************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAADRUlEQVR4nO2ZP0hVURzHDxIi4uggEdIUEiEhEiIiTiEhIiEhISTiIBIiDmIigoOIgzRINEhINESDQ5OIiEM0OERDQ0ODhENTRIiIyOPTcO/j/bvnz73nvPckfp/xvXu+3+/vnHvP/b3zlBIEQRAEQRAEQRCEawFwB9gHfgNfgIf1zlQzgBbgJ6VcAp3V8GuohqgnL5RS7WWfNSqlnlXD7FpNANCulJrXfN1Uyyx1AXiPnv97HwB6gZym+P16h2sAJoFdYAfoqoL+sab4K+Ceg0ZXnG03zhrm8Y7DbSfsyn1BDCKPp4Zb/7XD+L44UzHb3pMAKGBLE+zQS7zg0Qycajz+AK0OGoea8VuAV7gNw8qcZlcu8VgxeOjeCOUaugkE2MgabNUgCnCQSbjU4yZwptH/ATQ66uxbsq6mDbZgEbwEejJVXerz1uAxkkKnh8o9oJwFV7FZh+KHMldd8OlG/9o7IuWzCww5TMKsTWTKInAFjPoUHvso4JPGIwfcz6g7Gmc0MaUbPI5+RfLBxr0qLw2q442ndvo6vGYufcAm4ETjcQa0BfBwv5OBfnyfnXThFg0+iwF9XPayfgUcWC502z3dQrUBfzU+J0DQX3yWyQY4UOhvR4DP+HRSlYHKW+pingQzKvipuAYdJwrYs8zSeqAwneg3p6ATXeS5bqltL/8+vrBcuOwZxNSv54AHoYou8ly21HQBdOcvHsS+ETr15ZowIwbdd8GqLvjNW2q5BAbLBw07TMJMhjCNRH19EufArWCVR34zDsUP6waPYW8iJlIGMq3GSoiii7wmHPKbN1tHkTHHQK1Ev+mTOAWag1SuAi8eMG0QgqiTGnDQeWXQCNJWxz4D2DvZ6bSicxbBI8v4u4ZQx4Q6s4u8jixZ57IKLxlEjSdCmA8pejMF0nuZToSWfMXXNMJ7hjGPDIE+eAVK9tM1c2shxBWwWSZ8Tr6JqLz+BvBdE+gCuO0dqtKzO85UzCahust4Eh4Tnbu/BDoM1z43rL7/iuh9O+JsO3HWalkZQ7QQ/aWdxC+gpfapaghRS61jst75qg7R4UoSXwn42ru2kLwB5oD+emerGUA78DHeC76R4nxfEARBEARBEARBEGrDP7DttgMlnWLMAAAAAElFTkSuQmCC\"\n\n//# sourceURL=webpack:///./code.png?");

/***/ }),

/***/ "./people.js":
/*!*******************!*\
  !*** ./people.js ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar people = [{\n    manager: 'Jen',\n    name: 'Bob'\n}, {\n    manager: 'Jen',\n    name: 'Sue'\n}, {\n    manager: 'Bob',\n    name: 'Shirley'\n}, {\n    manager: 'Bob',\n    name: 'Terrence'\n}];\n\nmodule.exports = people;\n\n//# sourceURL=webpack:///./people.js?");

/***/ }),

/***/ "./style.scss":
/*!********************!*\
  !*** ./style.scss ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./style.scss?");

/***/ })

/******/ });