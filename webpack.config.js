// webpack.config.js
const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const config = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        app: './app.js',
        admin: './admin.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js',
        publicPath: '/dist/',
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: path.resolve(__dirname, 'src'),
            exclude: /(node_modules|bower_components)/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        'es2015',
                        'react'
                    ]
                }
            }],
            parser: {
                system: true // no warning
            }
        }, {
            test: /\.scss$/,
            use: [
                /*
                    sass-loader transforms Sass into CSS.
                    css-loader parses the CSS into JavaScript and resolves any dependencies.
                    style-loader outputs our CSS into a <style> tag in the document.
                    You can think of these as function calls, the output of one loader feeds as input into the next.

                    styleLoader(cssLoader(sassLoader('source')))
                */
                MiniCssExtractPlugin.loader,
                //'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }, {
            test: /\.css$/,
            use: [
                /*
                    sass-loader transforms Sass into CSS.
                    css-loader parses the CSS into JavaScript and resolves any dependencies.
                    style-loader outputs our CSS into a <style> tag in the document.
                    You can think of these as function calls, the output of one loader feeds as input into the next.

                    styleLoader(cssLoader(sassLoader('source')))
                */
                MiniCssExtractPlugin.loader,
                //'style-loader',
                'css-loader'
            ]
        }, {
            test: /\.(png|jpg|jpeg)$/,
            use: [{
                loader: 'url-loader',
                options: { limit: 10000 } // Convert images < 10k to base64 strings
            }]
        }]
    },
    mode: 'development',
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'common',
                    chunks: 'all'
                }
            }
        }
    },
    plugins: [
        new MiniCssExtractPlugin({
          // Options similar to the same options in webpackOptions.output
          // both options are optional
          filename: "[name].bundle.css",
          chunkFilename: "[id].css"
        }),
        new webpack.NamedModulesPlugin()
    ]
}

module.exports = config